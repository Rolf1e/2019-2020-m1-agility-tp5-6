// Test d'exemple par défaut :
describe('The Traffic web site home page', () => {
  it('successfully loads', () => {
    cy.visit('/')
  })
})


describe('Scenario 1', () => {
  it('Click and open configuration page', () => {
    cy.get('a[href="#configuration"]')
      .click();
    cy.hash()
      .should('eq', '#configuration');
  });
});

describe('Scenario 2', () => {
  it('There is 28 segments at start', () => {
    cy.get('a[href="#configuration"]')
      .click();
    cy.get('table[class="table table-sm table-striped table-bordered table-hover"]')
      .find('tr')
      .should('have.length', 29); //29 cause there is a table's headers
  });
});


describe('Scenario 3', () => {
  it('Click and open simulation page', () => {
    cy.get('a[href="#simulation"]')
      .click();
    cy.hash()
      .should('eq', '#simulation');
  });

  it('There is empty vehicle at start', () => {
    cy.get('table[class="table table-striped table-bordered table-hover"]')
      .contains('td', 'No vehicle available');
  });
});

describe('Scenario 4', () => {
  it('Update segment 5 speed', () => {
    cy.get('a[href="#configuration"]')
      .click();

    //Update field of segment form
    cy.get('input[form="segment-5"][type="number"]')
      .clear()
    cy.get('input[form="segment-5"][type="number"]')
      .type('30');

    //button
    cy.get('form[id="segment-5"]>button')
      .click();

    //Query api to verify input
    cy.request('http://127.0.0.1:4567/elements')
      .as('elements');

    cy.get('@elements').should((response) => {
      const rbody = response.body;
      const segments = rbody['segments'];
      const seg5 = segments[4];

      expect(seg5['speed'])
        .to.be.equal(30);

    });

    // Close opened window
    cy.get('div[class="modal-dialog"]>div>div[class="modal-footer"]>button')
      .click();
  });
});

describe('Scenario 5', () => {
  it('Update roundabout', () => {

    cy.get('a[href="#configuration"]')
      .click();

    //Capacity
    cy.get(':nth-child(3) > .card-body > form > :nth-child(2) > .form-control')
      .clear();
    cy.get(':nth-child(3) > .card-body > form > :nth-child(2) > .form-control')
      .type(4);

    //Duration
    cy.get(':nth-child(3) > .card-body > form > :nth-child(3) > .form-control')
      .clear();
    cy.get(':nth-child(3) > .card-body > form > :nth-child(3) > .form-control')
      .type(15);

    //button
    cy.get(':nth-child(3) > .card-body > form > .btn')
      .click();

    //Query api to verify input
    cy.request('http://127.0.0.1:4567/elements')
      .as('elements');

    cy.get('@elements').should((response) => {
      const rbody = response.body;
      const segments = rbody['crossroads'];
      const roundabout = segments[2];

      expect(roundabout['capacity'])
        .to.be.equal(4);

      expect(roundabout['duration'])
        .to.be.equal(15);
    });

    // Close opened window
    cy.get('div[class="modal-dialog"]>div>div[class="modal-footer"]>button')
      .click();

  });
});

describe('Scenario 6', () => {
  it('Traffic Light Update', () => {
    cy.get('a[href="#configuration"]')
      .click();

    //Origine
    cy.get(':nth-child(1) > .card-body > form > :nth-child(2) > .form-control')
      .clear();
    cy.get(':nth-child(1) > .card-body > form > :nth-child(2) > .form-control')
      .type(4);

    //Destination
    cy.get(':nth-child(1) > .card-body > form > :nth-child(3) > .form-control')
      .clear();
    cy.get(':nth-child(1) > .card-body > form > :nth-child(3) > .form-control')
      .type(40);

    //depart
    cy.get(':nth-child(1) > .card-body > form > :nth-child(4) > .form-control')
      .clear();
    cy.get(':nth-child(1) > .card-body > form > :nth-child(4) > .form-control')
      .type(8);

    cy.get(':nth-child(1) > .card-body > form > .btn')
      .click();

    //Query api to verify input
    cy.request('http://127.0.0.1:4567/elements')
      .as('elements');

    cy.get('@elements').should((response) => {
      const rbody = response.body;
      const segments = rbody['crossroads'];
      const trafficlight = segments[0];

      expect(trafficlight['id'])
        .to.be.equal(29);

      expect(trafficlight['orangeDuration'])
        .to.be.equal(4);

      expect(trafficlight['greenDuration'])
        .to.be.equal(40);

      expect(trafficlight['nextPassageDuration'])
        .to.be.equal(8);
    });

    // Close opened window
    cy.get('div[class="modal-dialog"]>div>div[class="modal-footer"]>button')
      .click();
  });
});


describe('Scenario 7 ', () => {
  it('Update Vehicle', () => {

    cy.get('a[href="#configuration"]')
      .click();

    addVehicle(5, 26, 50);
    addVehicle(19, 8, 200);
    addVehicle(27, 2, 150);

    //check server
    cy.request('http://127.0.0.1:4567/vehicles')
      .as('vehicles');

    cy.get('@vehicles').should((response) => {
      const rbody = response.body;

      expect(rbody["200.0"][0]["position"])
        .to.be.equal(0);

      expect(rbody["150.0"][0]["position"])
        .to.be.equal(0);

      expect(rbody["50.0"][0]["position"])
        .to.be.equal(0);
    });

    //check ui
    cy.get(':nth-child(4) > .col-md-8 > table > tbody')
      .find('tr')
      .should('have.length', 3);

  });
});

describe('Scenario 8 ', () => {
  it('Check vehicle dont move', () => {

    cy.get('a[href="#simulation"]')
      .click();
    //I really hope that the state from the last test isn't fucked up, because I use it

    //Get the table
    cy.get('.col-md-7 > .table > tbody > :nth-child(1) > :nth-child(3)')
      .contains('th', '0');

    cy.get('.col-md-7 > .table > tbody > :nth-child(2) > :nth-child(3)')
      .contains('th', '0');

    cy.get('.col-md-7 > .table > tbody > :nth-child(3) > :nth-child(3)')
      .contains('th', '0');

    //Simulation update
    cy.get('.form-control')
      .clear();
    cy.get('.form-control')
      .type(120);
    cy.get('.btn')
      .click();

    //Check progress bar
    cy.get('.progress-bar', { timeout: 100000 })
      .should('have.attr', 'aria-valuenow', '100');

    cy.get('.col-md-7 > .table > tbody > :nth-child(3) > :nth-child(3)')
      .contains('th', '10');

  });
});

describe('Scenario 9', () => {
  it('Reload and rerun simulation', () => {
    //Reload everything
    cy.reload();
    cy.get('a[href="#simulation"]')
      .click();

    cy.request('POST', 'http://127.0.0.1:4567/init')
      .as('init');

    cy.get('@init').should((response) => {
      expect(response.status).to.eq(200)
    });

    //Add vehicle
    cy.get('a[href="#configuration"]')
      .click();
    addVehicle(5, 26, 50);
    addVehicle(19, 8, 200);
    addVehicle(27, 2, 150);

    //launch simulation
    cy.get('a[href="#simulation"]')
      .click();
    //Simulation update
    cy.get('.form-control')
      .clear();
    cy.get('.form-control')
      .type(500);
    cy.get('.btn')
      .click();

    //Check progress bar
    cy.get('.progress-bar', { timeout: 1000000 })
      .should('have.attr', 'aria-valuenow', '100');

    //Check all vehicles are stopped at the end
    cy.get('.col-md-7 > .table > tbody > :nth-child(1) > :nth-child(3)')
      .contains('th', '0');
    cy.get('.col-md-7 > .table > tbody > :nth-child(2) > :nth-child(3)')
      .contains('th', '0');
    cy.get('.col-md-7 > .table > tbody > :nth-child(3) > :nth-child(3)')
      .contains('th', '0');

  });
});

describe('Scenario 10', () => {
  it('Check vehicles positions after 200 sec simulation ', () => {
    //Reload everything
    cy.reload();
    cy.get('a[href="#simulation"]')
      .click();

    cy.request('POST', 'http://127.0.0.1:4567/init')
      .as('init');

    cy.get('@init').should((response) => {
      expect(response.status).to.eq(200)
    });

    //Add vehicle
    cy.get('a[href="#configuration"]')
      .click();
    addVehicle(5, 26, 50);
    addVehicle(5, 26, 80);
    addVehicle(5, 26, 80);

    //Simulation update
    cy.get('a[href="#simulation"]')
      .click();
    cy.get('.form-control')
      .clear();
    cy.get('.form-control')
      .type(200);
    cy.get('.btn')
      .click();

    //Check progress bar
    cy.get('.progress-bar', { timeout: 100000 })
      .should('have.attr', 'aria-valuenow', '100');

    //Check vehicle positions
      cy.get('tbody > :nth-child(1) > :nth-child(5)')
      .contains('th', '29');
    cy.get('tbody > :nth-child(2) > :nth-child(5)')
      .contains('th', '29');
    cy.get('tbody > :nth-child(3) > :nth-child(5)')
      .contains('th', '17');
  });
});

function addVehicle(origine, destination, depart) {
  //origine
  cy.get('form > :nth-child(1) > .form-control')
    .clear();
  cy.get('form > :nth-child(1) > .form-control')
    .type(origine);

  //destination
  cy.get('.col-md-4 > form > :nth-child(2) > .form-control')
    .clear();
  cy.get('.col-md-4 > form > :nth-child(2) > .form-control')
    .type(destination);

  //depart
  cy.get('.col-md-4 > form > :nth-child(3) > .form-control')
    .clear();
  cy.get('.col-md-4 > form > :nth-child(3) > .form-control')
    .type(depart);

  //button
  cy.get('.col-md-4 > form > .btn')
    .click();

  // Close opened window
  cy.get('div[class="modal-dialog"]>div>div[class="modal-footer"]>button')
    .click();
}



